package fr.esgi.membership.payment.infrastructure;


import fr.esgi.membership.membership.infrastructure.MembershipEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "payment")
@Table(name = "payment")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaymentEntity {

    @Id
    @GeneratedValue
    private Integer id;

    private Float amount;

    @OneToOne
    @JoinColumn(name = "idMembership")
    private MembershipEntity idMembership;


}
