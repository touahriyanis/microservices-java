package fr.esgi.membership.payment.domain;

public interface PaymentDAO {
    Payment registerPayment(Payment payment);
}
