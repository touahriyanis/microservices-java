package fr.esgi.membership.payment.use_case;

import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.domain.MembershipState;
import fr.esgi.membership.payment.domain.Payment;
import fr.esgi.membership.payment.domain.PaymentDAO;
import fr.esgi.membership.payment.use_case.dto.PaymentDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class RegisterPayment {
    private final PaymentDAO paymentDAO;

    @Autowired
    public RegisterPayment(PaymentDAO paymentDAO) {
        this.paymentDAO = paymentDAO;
    }


    public Payment execute(PaymentDTO paymentDTO){
        final Payment payment= Payment.builder().idMembership(paymentDTO.getIdMembership())
                .amount(paymentDTO.getAmount()).build();

        return paymentDAO.registerPayment(payment);
    }
}
