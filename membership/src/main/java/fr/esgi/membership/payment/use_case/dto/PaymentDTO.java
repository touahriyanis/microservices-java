package fr.esgi.membership.payment.use_case.dto;

import fr.esgi.membership.membership.infrastructure.MembershipEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDTO {

    private Float amount;

    private MembershipEntity idMembership;
}

