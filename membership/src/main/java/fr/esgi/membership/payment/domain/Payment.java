package fr.esgi.membership.payment.domain;

import fr.esgi.membership.membership.infrastructure.MembershipEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Payment {

    private Integer id;

    private Float amount;

    private MembershipEntity idMembership;
}
