package fr.esgi.membership.payment.infrastructure;

import fr.esgi.membership.payment.domain.Payment;
import org.springframework.stereotype.Service;

@Service
public class PaymentAdapter {
    public Payment toDomain(PaymentEntity paymentEntity) {
            return new Payment(paymentEntity.getId(),paymentEntity.getAmount(),paymentEntity.getIdMembership());
    }

    public PaymentEntity toEntity(Payment payment) {
        return new PaymentEntity(payment.getId(),payment.getAmount(),payment.getIdMembership());
    }

}
