package fr.esgi.membership.payment.infrastructure;

import fr.esgi.membership.payment.domain.Payment;
import fr.esgi.membership.payment.domain.PaymentDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentDAOImpl implements PaymentDAO {

    private final PaymentRepository paymentRepository;

    private final PaymentAdapter paymentAdapter;

    @Autowired
    public PaymentDAOImpl(PaymentRepository paymentRepository, PaymentAdapter paymentAdapter) {
        this.paymentRepository = paymentRepository;
        this.paymentAdapter = paymentAdapter;
    }


    @Override
    public Payment registerPayment(Payment payment) {
        final PaymentEntity paymentEntity= paymentAdapter.toEntity(payment);
        paymentRepository.save(paymentEntity);
        return paymentAdapter.toDomain(paymentEntity);
    }
}
