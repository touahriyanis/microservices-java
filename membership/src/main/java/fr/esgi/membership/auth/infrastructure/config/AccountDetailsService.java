package fr.esgi.membership.auth.infrastructure.config;


import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.use_case.FindMembershipByUsername;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class AccountDetailsService implements UserDetailsService {

    private final FindMembershipByUsername findAccountByUsername;

    public AccountDetailsService(FindMembershipByUsername findAccountByUsername) {
        this.findAccountByUsername = findAccountByUsername;
    }


    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Membership account = findAccountByUsername.execute(username);


        return User.builder()
                .username(account.getEmail())
                .password(account.getPassword())
                .roles(account.getType().name())
                .accountExpired(false)
                .accountLocked(false)
                .credentialsExpired(false)
                .disabled(false)
                .build();
    }


}


