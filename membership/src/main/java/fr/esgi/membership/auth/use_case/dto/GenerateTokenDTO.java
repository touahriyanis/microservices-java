package fr.esgi.membership.auth.use_case.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;


@Data
@AllArgsConstructor
public class GenerateTokenDTO {

    Integer idMembership;

    String authorities;



}
