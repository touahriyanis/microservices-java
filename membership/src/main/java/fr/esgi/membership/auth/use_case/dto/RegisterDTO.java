package fr.esgi.membership.auth.use_case.dto;
import fr.esgi.membership.membership.domain.MembershipType;
import lombok.Data;

@Data
public class RegisterDTO {

    private final String username;

    private final String password;

    private final MembershipType role;

}
