package fr.esgi.membership.auth.use_case;


import fr.esgi.membership.auth.use_case.dto.MembershipDTO;
import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.domain.MembershipDAO;
import fr.esgi.membership.membership.domain.MembershipState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class Register {

    private final MembershipDAO membershipDAO;

    @Autowired
    public Register(MembershipDAO membershipDAO) {
        this.membershipDAO = membershipDAO;
    }

    public Membership execute(MembershipDTO membershipDTO){
        final Membership membership= Membership.builder()
                .firstName(membershipDTO.getFirstName())
                .lastName(membershipDTO.getLastName())
                .status(MembershipState.PENDING)
                .email(membershipDTO.getEmail())
                .password(membershipDTO.getPassword())
                .paid(false)
                .type(membershipDTO.getType())
                .available(true)
                .complexityLevel(membershipDTO.getComplexityLevel())
                .build();
        return membershipDAO.createMembership(membership);
    }
}
