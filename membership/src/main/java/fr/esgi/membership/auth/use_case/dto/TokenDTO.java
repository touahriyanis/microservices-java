package fr.esgi.membership.auth.use_case.dto;

import lombok.Data;

@Data
public class TokenDTO {
    private final String token;
}
