package fr.esgi.membership.auth.use_case;

import fr.esgi.membership.auth.use_case.dto.GenerateTokenDTO;
import fr.esgi.membership.auth.use_case.dto.LoginDTO;
import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.use_case.FindMembershipByUsername;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;


@Service
@Slf4j
public class Login {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final FindMembershipByUsername findMembershipByUsername;

    public Login(
            AuthenticationManagerBuilder authenticationManagerBuilder,
            FindMembershipByUsername findMembershipByUsername) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.findMembershipByUsername = findMembershipByUsername;

    }

    public String execute(LoginDTO loginDTO) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(loginDTO.getEmail(), loginDTO.getPassword());

        Membership account = findMembershipByUsername.execute(loginDTO.getEmail());

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);

        String authorities = authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(","));

        try {
            GenerateTokenDTO generateTokenDTO= new GenerateTokenDTO(account.getIdMembership(),authorities);
            RestTemplate restTemplate= new RestTemplate();

            ResponseEntity<Void> response =restTemplate.postForEntity("http://localhost:8083/auth/token",generateTokenDTO,Void.class);

            return response.getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
        }catch (Exception e){
            return null;
        }

    }
}
