package fr.esgi.membership.auth.use_case.dto;

import fr.esgi.membership.membership.domain.MembershipType;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class MembershipDTO {

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private MembershipType type;

    private Integer complexityLevel;

}
