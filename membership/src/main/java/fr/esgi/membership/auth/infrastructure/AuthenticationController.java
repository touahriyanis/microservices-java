package fr.esgi.membership.auth.infrastructure;


import fr.esgi.membership.auth.use_case.Login;
import fr.esgi.membership.auth.use_case.Register;
import fr.esgi.membership.auth.use_case.dto.MembershipDTO;
import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.auth.use_case.dto.LoginDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping("/auth")
@RestController
public class AuthenticationController {

    private final Register register;
    private final Login login;
    @Autowired
    public AuthenticationController(Register register, Login login) {
        this.register = register;
        this.login = login;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register( @RequestBody MembershipDTO membershipDTO){
        final Membership membership= register.execute(membershipDTO);
        return ResponseEntity.ok(membership) ;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginDTO loginDTO) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Access-Control-Expose-Headers", "Authorization");
        String token = login.execute(loginDTO);
        httpHeaders.add(HttpHeaders.AUTHORIZATION, token);
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }

}
