package fr.esgi.membership.auth.use_case.dto;

import lombok.Data;

@Data
public class LoginDTO {

    private final String email;

    private final String password;

}
