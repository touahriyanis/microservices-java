package fr.esgi.membership.use_case;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.membership.membership.domain.MembershipDAO;
import fr.esgi.membership.membership.infrastructure.kafka.MembershipNotificationProducer;
import fr.esgi.membership.membership.infrastructure.kafka.Topics;
import fr.esgi.membership.use_case.dtos.ProjectTradesmanDto;
import fr.esgi.membership.use_case.dtos.TradesmanAssignDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class IsisTradesmanEligibleForProjectAssignmentListener {

    private static final Logger logger = LoggerFactory.getLogger(IsisTradesmanEligibleForProjectAssignmentListener.class);

    private final MembershipNotificationProducer notificationProducer;
    private final MembershipDAO membershipDAO;
    ObjectMapper objectMapper = new ObjectMapper();

    @KafkaListener(topics = Topics.IS_TRADESMAN_AVAILABLE_FOR_PROJECT_ASSIGNMENT,groupId = "group_id")
    public void isTradesmanEligibleForProjectAssignment(String projectTradesmanDto){
        try {
            ProjectTradesmanDto dto = objectMapper.readValue(projectTradesmanDto, ProjectTradesmanDto.class);
            var isEligible = membershipDAO.isTradesmanEligible(dto.getTradesmanId());

            notificationProducer.NotifyAssignTradesmanToProjectIfAvailable(
                    new TradesmanAssignDto(isEligible, dto.getTradesmanId(), dto.getProjectId())
            );

            logger.info("tradesman " + dto.getTradesmanId() + " eligibility : " + isEligible);
            logger.info("message to assign him to project " + dto.getProjectId() + " is sent");

        } catch(Exception e) {
            System.out.println("couldn't assign tradesman");
        }


    }

}
