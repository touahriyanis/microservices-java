package fr.esgi.membership.use_case.dtos;

import lombok.Data;

@Data
public class TradesmanReleaseDto {
    private Integer tradesmanId;
    private Integer projectId;
}
