package fr.esgi.membership.use_case.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TradesmanAssignDto {
    private boolean isEligible;
    private Integer tradesmanId;
    private Integer projectId;
}
