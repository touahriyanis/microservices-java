package fr.esgi.membership.use_case;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.membership.membership.domain.MembershipDAO;
import fr.esgi.membership.membership.infrastructure.kafka.MembershipNotificationProducer;
import fr.esgi.membership.membership.infrastructure.kafka.Topics;
import fr.esgi.membership.use_case.dtos.TradesmanReleaseDto;
import fr.esgi.membership.use_case.dtos.TradesmanReleaseNotificationDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TerminateTradesmanListener {
    private static final Logger logger = LoggerFactory.getLogger(TerminateTradesmanListener.class);

    private final MembershipNotificationProducer notificationProducer;
    private final MembershipDAO membershipDAO;
    ObjectMapper jsonMapper = new ObjectMapper();

    @KafkaListener(topics = Topics.RELEASE_TRADESMAN_FROM_PROJECT,groupId = "group_id")
    public void releaseTradesman(String tradesmanReleaseDto){
        try {
            TradesmanReleaseDto dto = jsonMapper.readValue(tradesmanReleaseDto, TradesmanReleaseDto.class);

            var terminatedTradesman = membershipDAO.terminateTradesman(dto.getTradesmanId());
            var successfullyTerminated = terminatedTradesman != null;

            notificationProducer.NotifyTradesmanReleaseIfTerminated(
                    new TradesmanReleaseNotificationDto(dto.getTradesmanId(), dto.getProjectId(), successfullyTerminated )
            );

            logger.info("tradesman " + dto.getTradesmanId() + " released : " + successfullyTerminated);
            logger.info("message to terminate from project " + dto.getProjectId() + " is sent");

        } catch(Exception e) {
            logger.info("couldn't release tradesman due to an error");
        }
    }
}
