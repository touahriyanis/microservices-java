package fr.esgi.membership.membership.infrastructure.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.membership.use_case.dtos.TradesmanReleaseNotificationDto;
import fr.esgi.membership.use_case.dtos.TradesmanAssignDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class MembershipNotificationProducer {

    private static final Logger logger = LoggerFactory.getLogger(MembershipNotificationProducer.class);
    private static final ObjectMapper jsonMapper = new ObjectMapper();
    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;


    public void NotifyAssignTradesmanToProjectIfAvailable(TradesmanAssignDto tradesmanAssignDto){
        try {
            var msg = jsonMapper.writeValueAsString(tradesmanAssignDto);
            this.kafkaTemplate.send(Topics.ASSIGN_TRADESMAN_TO_PROJECT_IF_ELIGIBLE,msg);
        } catch (Exception e) {
            logger.info("couldnt assign tradesman due to an error");
        }
    }

    public void NotifyTradesmanReleaseIfTerminated(TradesmanReleaseNotificationDto tradesmanReleaseNotificationDto){
        try {
            var msg = jsonMapper.writeValueAsString(tradesmanReleaseNotificationDto);
            this.kafkaTemplate.send(Topics.RELEASE_TRADESMAN_FROM_PROJECT_NOTIFICATION,msg);
        } catch (Exception e) {
            logger.info("couldnt terminate tradesman due to an error");
        }
    }
}
