package fr.esgi.membership.membership.infrastructure;

import fr.esgi.membership.membership.domain.MembershipState;
import fr.esgi.membership.membership.domain.MembershipType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MembershipRepository  extends JpaRepository<MembershipEntity, Integer> {

    List<MembershipEntity>  findByAvailableIsTrueAndStatusIsLikeAndPaidIsTrueAndComplexityLevelGreaterThanEqualAndTypeLike(MembershipState status, Integer complexityLevel, MembershipType type);

    Optional<MembershipEntity> findByEmail(String email);

}
