package fr.esgi.membership.membership.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Membership {

    private Integer idMembership;

    private String firstName;

    private String lastName;

    private MembershipState status;

    private String email;

    private String password;

    private Boolean paid;

    private MembershipType type;

    private Boolean available ;

    private Integer complexityLevel;

}
