package fr.esgi.membership.membership.use_case.dto;

import lombok.Data;

@Data
public class MatchableTradesmenDto {
    private Integer complexityLevel;
}
