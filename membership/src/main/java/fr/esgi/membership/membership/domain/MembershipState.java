package fr.esgi.membership.membership.domain;

public enum MembershipState {
    PENDING,
    ACCEPTED,
    REFUSED,
}
