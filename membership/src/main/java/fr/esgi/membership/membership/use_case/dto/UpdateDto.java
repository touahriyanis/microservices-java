package fr.esgi.membership.membership.use_case.dto;

import fr.esgi.membership.membership.domain.MembershipState;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateDto {

        private Boolean paid;

        private MembershipState status;

}
