package fr.esgi.membership.membership.infrastructure;

import fr.esgi.membership.membership.domain.MembershipState;
import fr.esgi.membership.membership.domain.MembershipType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity(name = "Membership")
@Table(name = "membership")
@Data
@NoArgsConstructor
@AllArgsConstructor
public  class MembershipEntity{
    @Id
    @GeneratedValue
    private Integer idMembership;

    private String firstName;

    private String lastName;

    @Enumerated(EnumType.STRING)
    private MembershipState status;

    private String email;

    private String password;

    private Boolean paid ;

    @Enumerated(EnumType.STRING)
    private MembershipType type;

    private Integer complexityLevel;

    private Boolean available ;


}