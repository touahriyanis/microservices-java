package fr.esgi.membership.membership.domain;

public enum MembershipType {
    TRADESMAN,
    CONTRACTOR
}
