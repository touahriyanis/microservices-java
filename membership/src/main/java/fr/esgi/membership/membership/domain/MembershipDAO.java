package fr.esgi.membership.membership.domain;


import java.util.Optional;

public interface MembershipDAO {

    Membership createMembership(Membership membership);

    Membership updateMembership(Membership membership);

    Membership getBestFitTradesman(Integer complexityLevel);

    Membership assignTradesman(Integer tradesmanId);

    Membership terminateTradesman(Integer tradesmanId);

    boolean isTradesmanEligible(Integer tradesmanId);

    Optional<Membership> findByUsername(String username);
}
