package fr.esgi.membership.membership.infrastructure;

import fr.esgi.membership.membership.domain.Membership;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class MembershipAdapter {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public MembershipAdapter(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Membership toDomain(MembershipEntity membershipEntity) {
        return  new Membership(membershipEntity.getIdMembership(),membershipEntity.getFirstName(),
                membershipEntity.getLastName(),membershipEntity.getStatus(),membershipEntity.getEmail(),membershipEntity.getPassword(),membershipEntity.getPaid(),membershipEntity.getType(),membershipEntity.getAvailable(),membershipEntity.getComplexityLevel());
    }

    public MembershipEntity toEntity(Membership membership) {
        return new MembershipEntity(membership.getIdMembership(),membership.getFirstName(),membership.getLastName(),membership.getStatus(),membership.getEmail(),passwordEncoder.encode(membership.getPassword()),membership.getPaid(),membership.getType(),membership.getComplexityLevel(),membership.getAvailable());
    }

}
