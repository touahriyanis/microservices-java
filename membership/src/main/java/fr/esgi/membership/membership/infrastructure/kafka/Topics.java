package fr.esgi.membership.membership.infrastructure.kafka;

public final class Topics {
    public static final String ASSIGN_TRADESMAN_TO_PROJECT_IF_ELIGIBLE = "AssignTradesmanToProjectIfEligible";
    public static final String IS_TRADESMAN_AVAILABLE_FOR_PROJECT_ASSIGNMENT = "isTradesmanEligibleForProjectAssignment";
    public static final String RELEASE_TRADESMAN_FROM_PROJECT = "releaseTradesmanFromProject";
    public static final String RELEASE_TRADESMAN_FROM_PROJECT_NOTIFICATION = "releaseTradesmanFromProjectNotification";
}
