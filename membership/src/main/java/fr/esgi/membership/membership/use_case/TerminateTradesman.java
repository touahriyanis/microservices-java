package fr.esgi.membership.membership.use_case;

import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.domain.MembershipDAO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TerminateTradesman {

    private final MembershipDAO membershipDAO;

    public Membership execute(Integer tradesmanId){
        return  membershipDAO.terminateTradesman(tradesmanId);
    }
}
