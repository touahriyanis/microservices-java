package fr.esgi.membership.membership.use_case;

import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.domain.MembershipDAO;
import fr.esgi.membership.membership.use_case.dto.UpdateDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateMembership {
    private final MembershipDAO membershipDAO;

    @Autowired
    public UpdateMembership(MembershipDAO membershipDAO) {
        this.membershipDAO = membershipDAO;
    }

    public Membership execute(Integer idMembership, UpdateDto updateDto){
        final Membership membership= Membership.builder()
                .idMembership(idMembership)
                .status(updateDto.getStatus())
                .paid(updateDto.getPaid()).build();
        return membershipDAO.updateMembership(membership);
    }
}
