package fr.esgi.membership.membership.infrastructure;

import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.domain.MembershipDAO;
import fr.esgi.membership.membership.domain.MembershipState;
import fr.esgi.membership.membership.domain.MembershipType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MembershipDAOImpl implements MembershipDAO {

    private final MembershipRepository membershipRepository;

    private final MembershipAdapter membershipAdapter;

    @Autowired
    public MembershipDAOImpl(MembershipRepository membershipRepository, MembershipAdapter membershipAdapter) {
        this.membershipRepository = membershipRepository;
        this.membershipAdapter = membershipAdapter;
    }

    public Optional<Membership> getMembershipById(Integer id){
        final Optional<MembershipEntity> membershipEntity= membershipRepository.findById(id);
        return membershipEntity.map(membershipAdapter::toDomain);
    }

    @Override
    public Membership createMembership(Membership membership) {
        final MembershipEntity membershipEntity= membershipAdapter.toEntity(membership);
        membershipRepository.save(membershipEntity);
        return membershipAdapter.toDomain(membershipEntity);
    }

    @Override
    public Membership updateMembership(Membership membership){
        getMembershipById(membership.getIdMembership()).ifPresent(membership1 -> {
            membership.setEmail(membership1.getEmail())
                    .setIdMembership(membership1.getIdMembership())
                    .setFirstName(membership1.getFirstName())
                    .setPassword(membership1.getPassword())
                    .setLastName(membership1.getLastName());
        });
        final MembershipEntity membershipEntity= membershipAdapter.toEntity(membership);
        membershipRepository.save(membershipEntity);
        return membershipAdapter.toDomain(membershipEntity);
    }

    @Override
    public Membership getBestFitTradesman(Integer complexityLevel) {
       var membershipEntity =membershipRepository.findByAvailableIsTrueAndStatusIsLikeAndPaidIsTrueAndComplexityLevelGreaterThanEqualAndTypeLike(MembershipState.ACCEPTED,complexityLevel, MembershipType.TRADESMAN);
       return getBestTradesmanFit(membershipEntity.stream().map(membershipAdapter::toDomain).collect(Collectors.toList()), complexityLevel);
    }

    @Override
    public Membership assignTradesman(Integer tradesmanId) {
        final Optional<MembershipEntity> membershipEntity= membershipRepository.findById(tradesmanId);
        if(membershipEntity.isPresent()) {
            var entity = membershipEntity.get();
            entity.setAvailable(false);
            membershipRepository.save(entity);
            return membershipAdapter.toDomain(entity);
        }
        return null;
    }

    @Override
    public Membership terminateTradesman(Integer tradesmanId) {
        final Optional<MembershipEntity> membershipEntity= membershipRepository.findById(tradesmanId);
        if(membershipEntity.isPresent()) {
            var entity = membershipEntity.get();
            entity.setAvailable(true);
            membershipRepository.save(entity);
            return membershipAdapter.toDomain(entity);
        }
        return null;
    }

    @Override
    public boolean isTradesmanEligible(Integer tradesmanId) {
        final Optional<Membership> membership= membershipRepository.findById(tradesmanId).map(membershipAdapter::toDomain);
        if(membership.isPresent()) {
            var entity = membership.get();
            return entity.getAvailable();
        }
        return false;
    }

    @Override
    public Optional<Membership> findByUsername(String username) {
        return membershipRepository.findByEmail(username)
                .map(membershipAdapter::toDomain);
    }

    public Membership getBestTradesmanFit(List<Membership> members, Integer complexityLevel) {
        var member = members.stream().min(Comparator.comparingInt(Membership::getComplexityLevel));
        return member.orElse(null);
    }

}
