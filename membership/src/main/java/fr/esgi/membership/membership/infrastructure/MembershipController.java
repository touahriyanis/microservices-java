package fr.esgi.membership.membership.infrastructure;

import fr.esgi.membership.membership.domain.Membership;
import fr.esgi.membership.membership.use_case.GetBestMatchableTradesmen;
import fr.esgi.membership.membership.use_case.TerminateTradesman;
import fr.esgi.membership.membership.use_case.UpdateMembership;
import fr.esgi.membership.membership.use_case.dto.MatchableTradesmenDto;
import fr.esgi.membership.membership.use_case.dto.UpdateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/memberships")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MembershipController {

    private final UpdateMembership updateMembership;

    private final GetBestMatchableTradesmen getBestMatchableTradesmen;

    private final TerminateTradesman terminateTradesman;

    @PutMapping("/{idMembership}")
    public ResponseEntity<Membership> updateMembership(@PathVariable Integer idMembership, @RequestBody UpdateDto updateDto){
        final Membership membership= updateMembership.execute(idMembership,updateDto);
        return ResponseEntity.ok(membership) ;
    }

    @PostMapping("/search/best-fit-tradesman")
    public ResponseEntity<Membership> getMatchableTradesmen(@RequestBody MatchableTradesmenDto matchableTradesmenDto){
        var tradesman = getBestMatchableTradesmen.execute(matchableTradesmenDto.getComplexityLevel());
        return ResponseEntity.ok(tradesman);
    }

    @GetMapping("/terminate-tradesman/{idMembership}")
    public ResponseEntity<Membership> terminateTradesman(@PathVariable Integer idMembership){
        var tradesman = terminateTradesman.execute(idMembership);
        return ResponseEntity.ok(tradesman);
    }


}
