package fr.esgi.market.market.use_case;

import fr.esgi.market.market.domain.port.ProjectDao;
import fr.esgi.market.market.infrastructure.kafka.MarketKafkaProducer;
import fr.esgi.market.use_case.dtos.ProjectTradesmanDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AssignTradesman {

    private static final Logger logger = LoggerFactory.getLogger(AssignTradesman.class);

    private final MarketKafkaProducer marketKafkaProducer;

    public void execute(ProjectTradesmanDto projectTradesmanDto) {

        marketKafkaProducer.isTradesmanEligibleForProjectAssignment(projectTradesmanDto);
    }
}
