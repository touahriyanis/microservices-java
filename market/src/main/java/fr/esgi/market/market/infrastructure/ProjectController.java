package fr.esgi.market.market.infrastructure;

import fr.esgi.market.market.domain.model.Project;
import fr.esgi.market.market.use_case.AssignTradesman;
import fr.esgi.market.market.use_case.CloseProject;
import fr.esgi.market.market.use_case.CreateProject;
import fr.esgi.market.market.use_case.ReleaseTradesman;
import fr.esgi.market.market.use_case.dto.ProjectDto;
import fr.esgi.market.use_case.dtos.ProjectTradesmanDto;
import fr.esgi.market.use_case.dtos.TradesmanReleaseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class ProjectController {

    private final CreateProject createProject;
    private final CloseProject closeProject;
    private final ReleaseTradesman releaseTradesman;
    private final AssignTradesman assignTradesman;

    @PostMapping
    public ResponseEntity<Project> createProject(@RequestBody ProjectDto projectDto){
        return ResponseEntity.ok(createProject.execute(projectDto));
    }

    @PutMapping("/close/{projectId}")
    public ResponseEntity<Project> closeProject(@PathVariable Integer projectId){
        var project = closeProject.execute(projectId);
        return ResponseEntity.ok(project);
    }

    @PutMapping("/release")
    public ResponseEntity<Project> releaseTradesman(@RequestBody TradesmanReleaseDto tradesmanReleaseDto){
        releaseTradesman.execute(tradesmanReleaseDto);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/assign")
    public ResponseEntity<Project> releaseTradesman(@RequestBody ProjectTradesmanDto projectTradesmanDto){
        assignTradesman.execute(projectTradesmanDto);
        return ResponseEntity.ok().build();
    }
}
