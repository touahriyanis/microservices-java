package fr.esgi.market.market.domain.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.Accessors;

@Data
@RequiredArgsConstructor
@Accessors(chain = true)
public class Project {
    private Integer projectId;
    private String projectName;
    private Integer complexityLevel;
    private String location;
    private Integer duration;
    private boolean isActiveProject;
    private Integer tradesmanId;
}
