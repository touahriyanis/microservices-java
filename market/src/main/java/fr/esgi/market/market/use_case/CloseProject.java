package fr.esgi.market.market.use_case;

import fr.esgi.market.market.domain.model.Project;
import fr.esgi.market.market.domain.port.ProjectDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CloseProject {

    private final ProjectDao projectDao;

    public Project execute(Integer projectId){
        var project =  projectDao.closeProject(projectId);
        return projectDao.releaseTradesman(project.getTradesmanId());
    }
}
