package fr.esgi.market.market.infrastructure.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.market.use_case.dtos.ProjectTradesmanDto;
import fr.esgi.market.use_case.dtos.TradesmanReleaseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class MarketKafkaProducer {
    private static final Logger logger = LoggerFactory.getLogger(MarketKafkaProducer.class);
    private static final ObjectMapper jsonMapper = new ObjectMapper();

    @Autowired
    private KafkaTemplate<String, String> releaseTradesmanKafkaTemplate;

    public void isTradesmanEligibleForProjectAssignment(ProjectTradesmanDto projectTradesmanDto) {
        try {
            String serializedProjectTradesmanDto = jsonMapper.writeValueAsString(projectTradesmanDto);
            releaseTradesmanKafkaTemplate.send(Topics.IS_TRADESMAN_AVAILABLE_FOR_PROJECT_ASSIGNMENT, serializedProjectTradesmanDto);
            logger.info("Sent message for isTradesmanEligibleForProjectAssignment for tradesman:" + projectTradesmanDto.getTradesmanId());
        } catch(Exception e) {
            System.out.println("failed");
        }
    }

    public void releaseTradesmanFromProject(TradesmanReleaseDto tradesmanReleaseDto) {
        try {
            String serializedTradesmanReleaseDto = jsonMapper.writeValueAsString(tradesmanReleaseDto);
            releaseTradesmanKafkaTemplate.send(Topics.RELEASE_TRADESMAN_FROM_PROJECT, serializedTradesmanReleaseDto);
            logger.info("Sent message for TradesmanRelease for tradesman:" + tradesmanReleaseDto.getTradesmanId());
        } catch(Exception e) {
            System.out.println("failed");
        }
    }
}
