package fr.esgi.market.market.use_case;

import fr.esgi.market.market.domain.model.Project;
import fr.esgi.market.market.domain.port.ProjectDao;
import fr.esgi.market.market.infrastructure.ProjectMapper;
import fr.esgi.market.market.use_case.dto.ProjectDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CreateProject {

    private final ProjectDao projectDao;
    private final ProjectMapper projectMapper;

    public Project execute(ProjectDto projectDto){
        var project = projectMapper.toDomain(projectDto).setActiveProject(true);
        return projectDao.createProject(project);
    }
}
