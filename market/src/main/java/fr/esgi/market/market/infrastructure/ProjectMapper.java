package fr.esgi.market.market.infrastructure;

import fr.esgi.market.market.domain.model.Project;
import fr.esgi.market.market.use_case.dto.ProjectDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class ProjectMapper {

    public Project toDomain(ProjectEntity projectEntity){
        return new ModelMapper().map(projectEntity, Project.class);
    }

    public ProjectEntity toEntity(Project project){
        return new ModelMapper().map(project, ProjectEntity.class);
    }

    public Project toDomain(ProjectDto projectDto){
        return new ModelMapper().map(projectDto, Project.class);
    }

}
