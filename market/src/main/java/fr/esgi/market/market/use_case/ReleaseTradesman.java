package fr.esgi.market.market.use_case;


import fr.esgi.market.market.infrastructure.kafka.MarketKafkaProducer;
import fr.esgi.market.use_case.dtos.TradesmanReleaseDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ReleaseTradesman {
    private static final Logger logger = LoggerFactory.getLogger(AssignTradesman.class);

    private final MarketKafkaProducer marketKafkaProducer;

    public void execute(TradesmanReleaseDto tradesmanReleaseDto) {
        marketKafkaProducer.releaseTradesmanFromProject(tradesmanReleaseDto);
    }

}
