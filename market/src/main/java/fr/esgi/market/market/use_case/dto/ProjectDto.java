package fr.esgi.market.market.use_case.dto;

import lombok.Data;

@Data
public class ProjectDto {

    private String projectName;
    private Integer complexityLevel;
    private String location;
    private Integer duration;
    private boolean isActiveProject;
    private Integer tradesmanId;
}
