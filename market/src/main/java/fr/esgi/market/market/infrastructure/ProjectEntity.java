package fr.esgi.market.market.infrastructure;

import com.sun.istack.Nullable;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "Project")
@Table(name = "project")
@Data
@RequiredArgsConstructor
public class ProjectEntity {
    @Id
    @GeneratedValue
    private Integer projectId;
    private String projectName;
    private Integer complexityLevel;
    private String location;
    private Integer duration;
    private boolean isActiveProject;
    @Nullable
    private Integer tradesmanId;
}
