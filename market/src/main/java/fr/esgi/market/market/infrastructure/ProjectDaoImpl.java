package fr.esgi.market.market.infrastructure;

import fr.esgi.market.market.domain.model.Project;
import fr.esgi.market.market.domain.port.ProjectDao;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class ProjectDaoImpl implements ProjectDao {

    private static final Logger logger = LoggerFactory.getLogger(ProjectDaoImpl.class);

    private final ProjectRepository projectRepository;
    private final ProjectMapper projectMapper;

    @Override
    public Project createProject(Project project) {
        return projectMapper.toDomain(
                projectRepository.save(
                        projectMapper.toEntity(project)));
    }

    @Override
    public Project closeProject(Integer projectId) {
        var projectEntity = projectRepository.findById(projectId);
        if (projectEntity.isPresent()) {
            var project = projectEntity.get();
            project.setActiveProject(false);
            return projectMapper.toDomain(projectRepository.save(project));
        }
        return null;
    }

    @Override
    public Project releaseTradesman(Integer tradesManId) {
        var projectEntity = projectRepository.findByTradesmanId(tradesManId);
        if (projectEntity.isPresent()) {
            var project = projectEntity.get();
            project.setTradesmanId(null);
            return projectMapper.toDomain(projectRepository.save(project));
        }
        return null;
    }

    @Override
    public Project assignTradesman(Integer tradesmanId, Integer projectId) {
        var projectEntity = projectRepository.findById(projectId);
        if (projectEntity.isPresent()) {
            var project = projectEntity.get();
            project.setTradesmanId(tradesmanId);
            return projectMapper.toDomain(projectRepository.save(project));
        }
        return null;
    }
}
