package fr.esgi.market.market.domain.port;

import fr.esgi.market.market.domain.model.Project;


public interface ProjectDao {
    Project createProject(Project project);
    Project closeProject(Integer projectId);

    Project releaseTradesman(Integer tradesMan);
    Project assignTradesman(Integer tradesmanId, Integer projectId);
}
