package fr.esgi.market.use_case;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.market.market.domain.port.ProjectDao;
import fr.esgi.market.market.infrastructure.kafka.Topics;
import fr.esgi.market.use_case.dtos.TradesmanAssignDto;
import fr.esgi.market.use_case.dtos.TradesmanReleaseNotificationDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ReleaseTradesmanIfTerminated {
    private static final Logger logger = LoggerFactory.getLogger(AssignTradesmanToProjectIfAvailable.class);

    private final ProjectDao projectDao;
    ObjectMapper jsonMapper = new ObjectMapper();

    @KafkaListener(topics = Topics.RELEASE_TRADESMAN_FROM_PROJECT_NOTIFICATION,groupId = "group_id")
    public void execute(String tradesmanReleaseDto){
        try {
            TradesmanReleaseNotificationDto dto = jsonMapper.readValue(tradesmanReleaseDto, TradesmanReleaseNotificationDto.class);
            if(dto.isSuccessful()) {
                projectDao.releaseTradesman(dto.getTradesmanId());
                logger.info("tradesman " + dto.getTradesmanId() + " is terminated and was released from project : " + dto.getProjectId());
            } else {
                logger.info("tradesman " + dto.getTradesmanId() + " was not released from project : " + dto.getProjectId());
            }
        } catch(JsonProcessingException e) {
            logger.info(e.getMessage());
        } catch(Exception e) {
            logger.info("couldn't release tradesman due to an error");
        }
    }
}
