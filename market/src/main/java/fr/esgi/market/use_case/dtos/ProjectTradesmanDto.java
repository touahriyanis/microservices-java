package fr.esgi.market.use_case.dtos;

import lombok.Data;

@Data
public class ProjectTradesmanDto {
    private Integer tradesmanId;
    private Integer projectId;
    private Integer complexityLevel;
}
