package fr.esgi.market.use_case;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esgi.market.market.domain.port.ProjectDao;
import fr.esgi.market.market.infrastructure.kafka.Topics;
import fr.esgi.market.use_case.dtos.ProjectTradesmanDto;
import fr.esgi.market.use_case.dtos.TradesmanAssignDto;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AssignTradesmanToProjectIfAvailable {

    private static final Logger logger = LoggerFactory.getLogger(AssignTradesmanToProjectIfAvailable.class);

    private final ProjectDao projectDao;
    ObjectMapper jsonMapper = new ObjectMapper();

    @KafkaListener(topics = Topics.ASSIGN_TRADESMAN_TO_PROJECT_IF_ELIGIBLE,groupId = "group_id")
    public void assignTradesmanToProjectIfAvailable(String projectTradesmanDto){
        try {
            TradesmanAssignDto dto = jsonMapper.readValue(projectTradesmanDto, TradesmanAssignDto.class);
            if(dto.isEligible()) {
                projectDao.assignTradesman(dto.getTradesmanId(), dto.getProjectId());
                logger.info("tradesman " + dto.getTradesmanId() + " is eligible and was assigned to project : " + dto.getProjectId());
            } else {
                logger.info("tradesman " + dto.getTradesmanId() + " is not eligible for project : " + dto.getProjectId());
            }
        } catch(JsonProcessingException e) {
            logger.info(e.getMessage());
        } catch(Exception e) {
            logger.info("couldn't assign tradesman due to an error");
        }
    }
}
