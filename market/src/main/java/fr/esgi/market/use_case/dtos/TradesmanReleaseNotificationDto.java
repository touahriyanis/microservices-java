package fr.esgi.market.use_case.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TradesmanReleaseNotificationDto {
    private Integer tradesmanId;
    private Integer projectId;
    private boolean isSuccessful;
}
