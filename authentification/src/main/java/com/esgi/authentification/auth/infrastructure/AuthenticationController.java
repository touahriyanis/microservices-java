package com.esgi.authentification.auth.infrastructure;

import com.esgi.authentification.auth.use_case.GenerateToken;

import com.esgi.authentification.auth.use_case.dto.GenerateTokenDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/auth")
@RestController
public class AuthenticationController {

    private final GenerateToken generateToken;

    @Autowired
    public AuthenticationController( GenerateToken generateToken) {
        this.generateToken = generateToken;
    }

    @PostMapping("/token")
    public ResponseEntity<?> generateToken(@RequestBody GenerateTokenDTO generateTokenDTO) {
        HttpHeaders httpHeaders = this.generateToken.execute(generateTokenDTO.getIdMembership(), generateTokenDTO.getAuthorities());
        return new ResponseEntity<>(httpHeaders, HttpStatus.OK);
    }

}
