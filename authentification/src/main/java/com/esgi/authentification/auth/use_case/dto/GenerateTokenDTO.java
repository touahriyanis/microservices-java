package com.esgi.authentification.auth.use_case.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;


@Data
@AllArgsConstructor
public class GenerateTokenDTO {

    Integer idMembership;

    String authorities;



}
