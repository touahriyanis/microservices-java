package com.esgi.authentification.auth.use_case;


import com.esgi.authentification.account.use_case.FindAccountByUsername;
import com.esgi.authentification.auth.infrastructure.config.TokenProvider;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class GenerateToken {

    private final TokenProvider tokenProvider;

    public GenerateToken(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    public HttpHeaders execute(Integer idMembership, String authorities) {
        String token = tokenProvider.createToken(authorities, idMembership);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Access-Control-Expose-Headers", "Authorization");

        //register token
        httpHeaders.add(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        return httpHeaders;
    }
}
