package com.esgi.authentification.account.use_case;


import com.esgi.authentification.account.domain.Account;
import com.esgi.authentification.account.domain.AccountDao;
import com.esgi.authentification.exception.AccountWithUsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FindAccountByUsername {

    private final AccountDao accountDao;


    public FindAccountByUsername(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public Account execute(String username){
        return accountDao.findByUsername(username)
                .orElse(null);
    }
}
