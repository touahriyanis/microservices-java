package com.esgi.authentification.account.use_case;


import com.esgi.authentification.account.domain.Account;
import com.esgi.authentification.account.domain.AccountDao;
import com.esgi.authentification.exception.ResourceWithIdNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FindAccountById {

    private final AccountDao accountDao;

    @Autowired
    public FindAccountById(AccountDao accountDao){
        this.accountDao = accountDao;
    }

    public Account execute(String id){
        return accountDao.findById(id)
                .orElse(null);
    }
}
