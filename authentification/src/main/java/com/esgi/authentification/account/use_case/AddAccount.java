package com.esgi.authentification.account.use_case;


import com.esgi.authentification.account.domain.Account;
import com.esgi.authentification.account.domain.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddAccount {

    private final AccountDao accountDao;

    @Autowired
    public AddAccount(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public String execute(Account account) {
        return accountDao.save(account);
    }
}
