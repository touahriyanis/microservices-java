package com.esgi.authentification.account.domain;

public enum Role {
    ADMIN,
    USER
}
