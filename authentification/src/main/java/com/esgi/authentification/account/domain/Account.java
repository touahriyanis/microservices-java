package com.esgi.authentification.account.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Account {

    private String id;
    private String username;
    private String password;
    private Role role;

}
