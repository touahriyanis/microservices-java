package com.esgi.authentification.account.use_case;


import com.esgi.authentification.account.domain.Account;
import com.esgi.authentification.account.domain.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FindAllAccounts {

    private final AccountDao accountDao;

    @Autowired
    public FindAllAccounts(AccountDao accountDao){
        this.accountDao = accountDao;
    }

    public List<Account> execute(){
        return accountDao.findAll();
    }
}
