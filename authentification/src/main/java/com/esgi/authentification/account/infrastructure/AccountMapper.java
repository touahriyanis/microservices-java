package com.esgi.authentification.account.infrastructure;

import com.esgi.authentification.account.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AccountMapper {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AccountMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public Account toDomain(AccountEntity accountEntity) {
        return new Account(accountEntity.getId(),accountEntity.getUsername(), accountEntity.getPassword(), accountEntity.getRole());
    }

    public AccountEntity toEntity(Account account) {
        return new AccountEntity(account.getId(),account.getUsername(),passwordEncoder.encode(account.getPassword()),account.getRole());
    }
}
