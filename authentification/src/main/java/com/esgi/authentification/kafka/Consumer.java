package com.esgi.authentification.kafka;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {
    @KafkaListener(topics = "membership",groupId = "group_id")
    public void consumeMessage(String message){
        System.out.println(message);
    }

    @KafkaListener(topics = "test_topic",groupId = "group_id")
    public void consumeMessageFromMicroservices(String message){
        System.out.println(message);
    }
}
