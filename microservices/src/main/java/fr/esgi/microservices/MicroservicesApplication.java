package fr.esgi.microservices;

import fr.esgi.microservices.infrastructure.dataprovider.entity.UserEntity;
import fr.esgi.microservices.infrastructure.dataprovider.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.List;

@SpringBootApplication
public class MicroservicesApplication {
    private final Logger logger = LoggerFactory.getLogger(MicroservicesApplication.class);
    @Autowired private UserRepository repository;

    @EventListener(ApplicationReadyEvent.class)
    public void runAfterStartup() {
        List allCustomers = this.repository.findAll();
        logger.info("Number of customers: " + allCustomers.size());

        UserEntity newCustomer = new UserEntity();
        newCustomer.setFirstName("John");
        newCustomer.setLastName("Doe");
        logger.info("Saving new customer...");
        this.repository.save(newCustomer);

        allCustomers = this.repository.findAll();
        logger.info("Number of customers: " + allCustomers.size());
    }

    public static void main(String[] args) {
        SpringApplication.run(MicroservicesApplication.class, args);
    }

}
