package fr.esgi.microservices.infrastructure.web;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@Validated
@RequestMapping("/api/custom")
@RequiredArgsConstructor
public class CustomController {

    @GetMapping()
    public ResponseEntity<String> isApiAvailable() {
        return ResponseEntity.ok("Api is working fine!!!!");
    }

}

