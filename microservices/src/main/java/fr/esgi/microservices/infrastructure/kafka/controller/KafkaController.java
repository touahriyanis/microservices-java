package fr.esgi.microservices.infrastructure.kafka.controller;

import fr.esgi.microservices.infrastructure.kafka.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kafkaapp")
public class KafkaController {
    @Autowired
    Producer producer;

    @PostMapping(value = "/post/{msg}")
    public ResponseEntity sendMessage(@PathVariable String msg){
        producer.sendMessage(msg);
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/postrani")
    public ResponseEntity sendMessageToMembership(){
        producer.sendMessageToMembership();
        return ResponseEntity.ok().build();
    }

}
