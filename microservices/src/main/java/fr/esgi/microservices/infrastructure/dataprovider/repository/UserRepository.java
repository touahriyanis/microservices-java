package fr.esgi.microservices.infrastructure.dataprovider.repository;

import fr.esgi.microservices.infrastructure.dataprovider.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {
}
