package fr.esgi.microservices.infrastructure.dataprovider.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "microservice_user")
@Data
public class UserEntity {
    @Id
    @GeneratedValue
    private long id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;
}
